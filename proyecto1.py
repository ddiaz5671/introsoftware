# Diego Alejandro Díaz
import threading
import sqlite3

# Crear o Conectar a la Base de Datos
conn = sqlite3.connect('registros.db')

# Crear una Tabla para las Tareas si no Existe
conn.execute('''CREATE TABLE IF NOT EXISTS registros
                (id INTEGER PRIMARY KEY AUTOINCREMENT,
                fecha TEXT NOT NULL,
                descripcion TEXT NOT NULL,
                dineroGastado TEXT NOT NULL);''')

# Cerrar la Conexión a la Base de Datos
conn.close()

# Semáforo
semaphore = threading.Semaphore(2)
# Mutex
mutex = threading.Lock()

# Capa de Presentación
def mostrarMenu():
    print("Opciones:")
    print("1. Mostrar Registros")
    print("2. Agregar Registro")
    print("3. Eliminar Registro")
    print("4. Filtrar Registros")
    print("5. Salir")

def mostrarRegistrosUI():
    registros = mostrarRegistrosLogica()
    print("Registros:")
    for i, (fecha, descripcion, dineroGastado) in enumerate(registros):
        print(f"{i + 1}. {fecha} - {descripcion} - S/.{dineroGastado}\n")
    print("\n")

def agregarRegistroUI():
    nuevoDia = int(input("Ingrese el nuevo día: "))
    nuevoMes = int(input("Ingrese el nuevo mes: "))
    nuevoAnio = int(input("Ingrese el nuevo anio: "))
    nuevaDescripcion = input("Ingrese la descripción: ")
    nuevoDineroGastado = input("Ingrese el dinero gastado S/.")
    print("\n")
    agregarRegistroLogica(nuevoDia, nuevoMes, nuevoAnio, nuevaDescripcion, nuevoDineroGastado)

def eliminarRegistroUI():
    indice = int(input("Ingrese el número del registro a eliminar: ")) - 1
    registroEliminado = eliminarRegistroLogica(indice)
    if (registroEliminado is not None):
        print(f"Se ha eliminado el registro número {indice + 1}, con fecha de '{registroEliminado}'\n")
    else:
        print("Indice Inválido\n")

def filtrarRegistrosUI():
    fechaAFiltrar = input("Ingrese la fecha de la que se quieran ver los registros: ")
    registros = filtrarRegistrosLogica(fechaAFiltrar)
    print("Gastos Registrados en la Fecha")
    for i, (descripcion, dineroGastado) in enumerate(registros):
        print(f"{i + 1}. {descripcion} - {dineroGastado}")
    print("\n")

# Capa de Lógica
def agregarRegistroLogica(nuevoDia, nuevoMes, nuevoAnio, nuevaDescripcion, nuevoDineroGastado):
    if ((nuevoDia > 0 and nuevoDia < 32) and (nuevoMes > 0 and nuevoMes < 13) and (nuevoAnio > 1950 and nuevoAnio < 2024)):
        nuevaFecha = str(nuevoDia) + "/" + str(nuevoMes) + "/" + str(nuevoAnio)
        agregarRegistro(nuevaFecha, nuevaDescripcion, nuevoDineroGastado)
    else: print("Fecha Inválida\n")

def mostrarRegistrosLogica():
    return mostrarRegistros()

def eliminarRegistroLogica(indice):
    return eliminarRegistro(indice)

def filtrarRegistrosLogica(fechaAFiltrar):
    return filtrarRegistros(fechaAFiltrar)

# Capa de Acceso de Datos
def conectardb():
    return sqlite3.connect('registros.db')

def agregarRegistro(nuevaFecha, nuevaDescripcion, nuevoDineroGastado):
    conn = conectardb()
    cursor = conn.cursor()
    cursor.execute("INSERT INTO registros (fecha, descripcion, dineroGastado) VALUES (?,?,?)", (nuevaFecha, nuevaDescripcion, nuevoDineroGastado))
    conn.commit()
    conn.close()

def mostrarRegistros():
    conn = conectardb()
    cursor = conn.cursor()
    cursor.execute("SELECT fecha, descripcion, dineroGastado FROM registros")
    registros = cursor.fetchall()
    conn.close()
    return registros

def eliminarRegistro(indice):
    conn = conectardb()
    cursor = conn.cursor()
    cursor.execute("SELECT id, fecha FROM registros")
    filas = cursor.fetchall()
    if 0 <= indice < len(filas):
        idRegistro, registroEliminado = filas[indice]
        cursor.execute("DELETE FROM registros WHERE id=?", (idRegistro,))
        conn.commit()
        conn.close()
        return registroEliminado
    else:
        conn.close()
        return None

def filtrarRegistros(fechaAFiltrar):
    conn = conectardb()
    cursor = conn.cursor()
    cursor.execute("SELECT descripcion, dineroGastado FROM registros WHERE descripcion=?", (fechaAFiltrar,))
    resgistros = cursor.fetchall()
    conn.close()
    return resgistros
    
# Interacción del Usuario
while True:
    mostrarMenu()

    opcion = input("Seleccione una opción: ")

    #print("Cargando...\n")
    with semaphore:
        if (opcion == "1"):
            mostrarRegistrosUI()
        elif (opcion == "2"):
            with mutex:
                agregarRegistroUI()
        elif (opcion == "3"):
            with mutex:
                eliminarRegistroUI()
        elif (opcion == "4"):
            filtrarRegistrosUI()
        elif (opcion == "5"):
            print("Hasta luego!")
            break
        else:
            print("Opción no válida. Intente nuevamente.")